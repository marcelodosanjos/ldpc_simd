# Dependencies
* OpenMP
* Intel MKL library
# Build

First and foremost, you must export an MKL_ROOT environnement variable containing the path to the MKL library:

`export MKL_ROOT=<path_to_intel_SDK>/compilers_and_libraries_2019.2.187/linux/mkl`

Then,

```
mkdir build
cd build
cmake ..
make
```
