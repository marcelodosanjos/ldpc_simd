#include <gtest/gtest.h>

#include "./Constantes/constantes_sse.h"
#include "CDecoder/MS/CDecoder_MS_fixed_flooded.h"

#define ITERATION_MIN 1
#define ITERATION_MAX 20
#define NB_BITS_VARIABLES    8
#define NB_BITS_MESSAGES     6
#define SAT_POS_VAR  ( (0x0001<<(NB_BITS_VARIABLES-1))-1)
#define SAT_NEG_VAR  (-(0x0001<<(NB_BITS_VARIABLES-1))+1)
#define SAT_POS_MSG  ( (0x0001<<(NB_BITS_MESSAGES -1))-1)
#define SAT_NEG_MSG  (-(0x0001<<(NB_BITS_MESSAGES -1))+1)

TEST(DecoderFloodedTest, ZeroedInput)
{
    char i_llr[_N];
    char o_llr[_N];

    CDecoder_MS_fixed_flooded dec;
    dec.setVarRange (SAT_NEG_VAR, SAT_POS_VAR);
    dec.setMsgRange (SAT_NEG_MSG, SAT_POS_MSG);

    for (int iter = ITERATION_MIN ; iter < ITERATION_MAX ; iter++)
    {
        std::fill_n(i_llr, _N, SAT_NEG_MSG);

        dec.decode(i_llr, o_llr, iter);

        for (int i = 0; i < _N ; i++)
        {
            ASSERT_EQ(o_llr[i], 0);
        }
    }
}

TEST(DecoderFloodedTest, ZeroedInputWithFirstFlippedBit)
{
    char i_llr[_N];
    char o_llr[_N];

    CDecoder_MS_fixed_flooded dec;
    dec.setVarRange (SAT_NEG_VAR, SAT_POS_VAR);
    dec.setMsgRange (SAT_NEG_MSG, SAT_POS_MSG);

    for (int iter = ITERATION_MIN ; iter < ITERATION_MAX ; iter++)
    {
        std::fill_n(i_llr, _N, SAT_NEG_MSG);
        i_llr[0] = SAT_POS_MSG;

        dec.decode(i_llr, o_llr, iter);

        for (int i = 0; i < _N ; i++)
        {
            ASSERT_EQ(o_llr[i], 0);
        }
    }
}

TEST(DecoderFloodedTest, ZeroedInputWithLastFlippedBit)
{
    char i_llr[_N];
    char o_llr[_N];

    CDecoder_MS_fixed_flooded dec;
    dec.setVarRange (SAT_NEG_VAR, SAT_POS_VAR);
    dec.setMsgRange (SAT_NEG_MSG, SAT_POS_MSG);

    for (int iter = ITERATION_MIN ; iter < ITERATION_MAX ; iter++)
    {
        std::fill_n(i_llr, _N, SAT_NEG_MSG);
        i_llr[_N-1] = SAT_POS_MSG;

        dec.decode(i_llr, o_llr, iter);

        for (int i = 0; i < _N ; i++)
        {
            ASSERT_EQ(o_llr[i], 0);
        }
    }
}

TEST(DecoderFloodedTest, ZeroedInputWithRandomFlippedBit)
{
    char i_llr[_N];
    char o_llr[_N];

    CDecoder_MS_fixed_flooded dec;
    dec.setVarRange (SAT_NEG_VAR, SAT_POS_VAR);
    dec.setMsgRange (SAT_NEG_MSG, SAT_POS_MSG);

    for (int iter = ITERATION_MIN ; iter < ITERATION_MAX ; iter++)
    {
        std::fill_n(i_llr, _N, SAT_NEG_MSG);
        i_llr[_N/4] = SAT_POS_MSG;

        dec.decode(i_llr, o_llr, iter);

        for (int i = 0; i < _N ; i++)
        {
            ASSERT_EQ(o_llr[i], 0);
        }
    }
}

int main(int argc, char* argv[])
{
    srand(0);

    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
