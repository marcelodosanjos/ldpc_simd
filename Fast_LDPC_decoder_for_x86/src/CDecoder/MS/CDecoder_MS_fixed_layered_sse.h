#ifndef CDECODER_MS_FIXED_LAYERED_SSE_H
#define CDECODER_MS_FIXED_LAYERED_SSE_H

#include <reds-dvb-decoder/reds-dvb-decoder.h>

#include "../template/CDecoder_fixed_layered.h"

class CDecoder_MS_fixed_layered_sse : public CDecoder_fixed_layered {
public:
    CDecoder_MS_fixed_layered_sse();
    ~CDecoder_MS_fixed_layered_sse();
    void decode(char var_nodes[], char Rprime_fix[], int nombre_iterations);

private:
    REDS_DVB_DECODER_Decoder_t _decoder;
};

#define SSE_16BIT_ELEM      8

#endif
