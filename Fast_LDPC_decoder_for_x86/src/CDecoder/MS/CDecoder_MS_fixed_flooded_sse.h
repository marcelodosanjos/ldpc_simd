/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of LDPC_C_Simulator.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CDECODER_MS_FIXED_FLOODED_SSE_H
#define CDECODER_MS_FIXED_FLOODED_SSE_H

#include <reds-dvb-decoder/reds-dvb-decoder.h>

#include "../template/CDecoder_fixed_flooded.h"

class CDecoder_MS_fixed_flooded_sse : public CDecoder_fixed_flooded {

public:
    CDecoder_MS_fixed_flooded_sse();
    ~CDecoder_MS_fixed_flooded_sse();
    void decode(char var_nodes[], char Rprime_fix[], int nombre_iterations);

private:
    REDS_DVB_DECODER_Decoder_t _decoder;
};

#endif
