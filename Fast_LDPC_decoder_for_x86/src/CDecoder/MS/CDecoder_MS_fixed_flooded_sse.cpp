/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of LDPC_C_Simulator.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CDecoder_MS_fixed_flooded_sse.h"

CDecoder_MS_fixed_flooded_sse::CDecoder_MS_fixed_flooded_sse()
{
    REDS_DVB_DECODER_Error_t ret;

    /* TODO coderates */
#ifdef USE_CODERATE_1_2
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_FLOODED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_32400);
#elif USE_CODERATE_8_9
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_FLOODED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_7200);
#elif USE_CODERATE_9_10
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_FLOODED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_6480);
#else
#error "Please specify coderate"
#endif /* USE_CODERATE_1_2 */

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Init returned " << ret << std::endl;
    }
}

CDecoder_MS_fixed_flooded_sse::~CDecoder_MS_fixed_flooded_sse()
{
    REDS_DVB_DECODER_Error_t ret = REDS_DVB_DECODER_Terminate(&_decoder);

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Terminate returned " << ret << std::endl;
    }
}

void CDecoder_MS_fixed_flooded_sse::decode(
    char var_nodes[],
    char Rprime_fix[],
    int nombre_iterations)
{
    REDS_DVB_DECODER_Error_t ret = REDS_DVB_DECODER_Decode(&_decoder, var_nodes, Rprime_fix, nombre_iterations);

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Init returned " << ret << std::endl;
    }
}
