#include "CDecoder_MS_fixed_layered_sse.h"

#include <emmintrin.h>
#include <tmmintrin.h>
#include <smmintrin.h>

CDecoder_MS_fixed_layered_sse::CDecoder_MS_fixed_layered_sse()
{
    REDS_DVB_DECODER_Error_t ret;

    /* TODO coderates */
#ifdef USE_CODERATE_1_2
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_LAYERED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_32400);
#elif USE_CODERATE_8_9
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_LAYERED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_7200);
#elif USE_CODERATE_9_10
    ret = REDS_DVB_DECODER_Init(&_decoder,
                          REDS_DVB_DECODER_SCHEDULER_LAYERED,
                          REDS_DVB_DECODER_CODE_DVB_S2_64800_6480);
#else
#error "Please specify coderate"
#endif /* USE_CODERATE_1_2 */

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Init returned " << ret << std::endl;
    }
}

CDecoder_MS_fixed_layered_sse::~CDecoder_MS_fixed_layered_sse()
{
    REDS_DVB_DECODER_Error_t ret = REDS_DVB_DECODER_Terminate(&_decoder);

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Terminate returned " << ret << std::endl;
    }
}

void CDecoder_MS_fixed_layered_sse::decode(
    char var_nodes[],
    char Rprime_fix[],
    int nombre_iterations)
{
    REDS_DVB_DECODER_Error_t ret = REDS_DVB_DECODER_Decode(&_decoder, var_nodes, Rprime_fix, nombre_iterations);

    if (ret != REDS_DVB_DECODER_ERROR_NONE) {
        std::cerr << "Error: REDS_DVB_DECODER_Init returned " << ret << std::endl;
    }
}
