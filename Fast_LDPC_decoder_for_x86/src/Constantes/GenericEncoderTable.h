#ifdef USE_CODERATE_1_2
#include "./64800x32400.dvb-s2/GenericEncoderTable.h"
#elif USE_CODERATE_8_9
#include "./64800x7200.dvb-s2/GenericEncoderTable.h"
#elif USE_CODERATE_9_10
#include "./64800x6480.dvb-s2/GenericEncoderTable.h"
#else
#error "Please specify coderate"
#endif // CODERATE_1_2
