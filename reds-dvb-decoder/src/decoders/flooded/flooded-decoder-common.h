/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of reds-dvb-decoder.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLOODED_DECODER_COMMON_H
#define FLOODED_DECODER_COMMON_H

typedef struct {
    char** p_vn_addr;
    char** p_cn_addr;
    size_t *p_cn_addrSize;
    char* vn_msgs;
    char* cn_msgs;
    char* vn_est;
} REDS_DVB_DECODER_FloodedDecoder_t;

#endif /* FLOODED_DECODER_COMMON_H */
