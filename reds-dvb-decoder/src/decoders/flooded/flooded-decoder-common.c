/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of reds-dvb-decoder.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Flooded implementation based on the following paper:
 *
 * Efficient GPU and CPU-based LDPC decoders for long codewords
 * Stefan Grönroos, Kristian Nybom, Jerker Björkqvist
 *
 * Analog Integr Circ Sig Process (2012) 73:583–595
 * DOI 10.1007/s10470-012-9895-7
 */

/* Warning: Do not compile me! Include me in coderate decoder. */
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>

#include <xmmintrin.h>
#include <emmintrin.h>
#include <smmintrin.h>

#include <omp.h>

#include "reds-dvb-decoder/reds-dvb-decoder.h"
#include "flooded-decoder-common.h"

#ifndef CODERATE_SUFFIX
#error "Error: CODERATE_SUFFIX is not defined"
#endif /* CODERATE_SUFFIX */

#define INIT_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_flooded_ ## x ## _Init
#define INIT_FUNCTION_NAME(x) INIT_FUNCTION_NAME_HELPER(x)

#define TERMINATE_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_flooded_ ## x ## _Terminate
#define TERMINATE_FUNCTION_NAME(x) TERMINATE_FUNCTION_NAME_HELPER(x)

#define DECODE_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_flooded_ ## x ## _Decode
#define DECODE_FUNCTION_NAME(x) DECODE_FUNCTION_NAME_HELPER(x)

// TODO comment style
// TODO variable name

/* TODO JKI check that DEG_1 > DEG_2 */
static const unsigned int cn_deg_max = DEG_1;
static const unsigned int vn_deg_max = DEG_VN_MAX;
static const size_t nb_vn = _N;

/* TODO JKI Use macro in table */
#define vSAT_POS_VAR 127
#define vSAT_NEG_VAR -127
#define vSAT_POS_MSG 127
#define vSAT_NEG_MSG -127

//const size_t CDecoder_MS_fixed_flooded::nb_vn = _N;
//const char CDecoder_MS_fixed_flooded::hard_decision_threshold = 0;
//
REDS_DVB_DECODER_Error_t INIT_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder)
{
    const size_t nb_edges = _M;
    const size_t nb_rows = _K;
    const size_t nb_cols = _N;
    size_t row;
    size_t vn_slot_offset, cn_slot_offset;
    size_t vn_idx;
    size_t* p_vn_addrSize;
    REDS_DVB_DECODER_Error_t ret = REDS_DVB_DECODER_ERROR_NONE;

    REDS_DVB_DECODER_FloodedDecoder_t* floodedDecoder = malloc(sizeof(REDS_DVB_DECODER_FloodedDecoder_t));
    decoder->decoder = floodedDecoder;

    // Allocate VNs and CNs messages
    floodedDecoder->vn_msgs = malloc(sizeof(*floodedDecoder->vn_msgs) * vn_deg_max * _N);
    floodedDecoder->cn_msgs = malloc(sizeof(*floodedDecoder->cn_msgs) * cn_deg_max * _K);

    // Allocation VN estimates
    floodedDecoder->vn_est = malloc(sizeof(*floodedDecoder->vn_est) * _N);

    // We allocate a max of cn_deg_max per CN
    floodedDecoder->p_vn_addr = malloc(sizeof(*floodedDecoder->p_vn_addr) * nb_rows * cn_deg_max);
    p_vn_addrSize = malloc(sizeof(*p_vn_addrSize) * nb_rows);

    for(size_t i = 0; i < nb_rows; i++) {
        p_vn_addrSize[i] = 0;
    }

    // We allocate a max of vn_deg_max per VN
    floodedDecoder->p_cn_addr = malloc(sizeof(*floodedDecoder->p_cn_addr) * nb_cols * vn_deg_max);
    floodedDecoder->p_cn_addrSize = malloc(sizeof(*floodedDecoder->p_cn_addrSize) * nb_cols);

    for(size_t i = 0; i < nb_cols; i++) {
        floodedDecoder->p_cn_addrSize[i] = 0;
    }

    /* Iterate over the entire matrix H */
    for(size_t edge_idx = 0; edge_idx < nb_edges; edge_idx++) {
        vn_idx = VN_NODE_ARRAY_NAME[edge_idx];

        /* The following line is quite problematic. We assume that all CNs
         * have the same degree like it is the case with a DVB-S2 LDPC code.
         * (excluding the last CN, but it's not a problem here).
         *
         * What if the code has variable degree per CN ?
         */
        row = edge_idx / cn_deg_max; // Also indicates the current CN

        // Associate vn addr to cn index, and cn addr to vn index
        vn_slot_offset = p_vn_addrSize[row];
        cn_slot_offset = floodedDecoder->p_cn_addrSize[vn_idx];

        if (p_vn_addrSize[row] < cn_deg_max) {
            p_vn_addrSize[row]++;
        } else {
            fprintf(stderr, "vn_addr offset out of bound");
            ret = REDS_DVB_DECODER_ERROR_RUNTIME;
            goto exit_error;
        }

        if (floodedDecoder->p_cn_addrSize[vn_idx] < vn_deg_max) {
            floodedDecoder->p_cn_addrSize[vn_idx]++;
        } else {
            fprintf(stderr, "cn_addr offset out of bound");
            ret = REDS_DVB_DECODER_ERROR_RUNTIME;
            goto exit_error;
        }

        floodedDecoder->p_vn_addr[row*cn_deg_max + vn_slot_offset] =
            &floodedDecoder->vn_msgs[vn_idx*vn_deg_max + cn_slot_offset];
        floodedDecoder->p_cn_addr[vn_idx*vn_deg_max + cn_slot_offset] =
            &floodedDecoder->cn_msgs[row*cn_deg_max + vn_slot_offset];
    }

    free(p_vn_addrSize);

    return ret;

exit_error:
    free(floodedDecoder->p_cn_addrSize);
    free(floodedDecoder->p_cn_addr);

    free(p_vn_addrSize);

    free(floodedDecoder->p_vn_addr);

    free(floodedDecoder->vn_est);

    free(floodedDecoder->cn_msgs);
    free(floodedDecoder->vn_msgs);

    free(decoder->decoder);
    decoder->decoder = NULL;

    return ret;
}

void TERMINATE_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder)
{
    REDS_DVB_DECODER_FloodedDecoder_t* floodedDecoder = decoder->decoder;

    free(floodedDecoder->p_vn_addr);
    free(floodedDecoder->p_cn_addr);
    free(floodedDecoder->p_cn_addrSize);
    free(floodedDecoder->vn_est);
    free(floodedDecoder->vn_msgs);
    free(floodedDecoder->cn_msgs);

    free(decoder->decoder);
    decoder->decoder = NULL;
}

static inline void computeCnMsgsDeg1(REDS_DVB_DECODER_FloodedDecoder_t* floodedDecoder)
{
    /* SSE constants */
    const __m128i vzero = _mm_set1_epi8(0);

    /* Loop constants */
    const size_t firstCnVectorIdx = 0;
    const size_t lastCnVectorIdx = (DEG_1_COMPUTATIONS/16)*16;
    const size_t lastCnIdx = DEG_1_COMPUTATIONS;

    /* Check node updates.
     * For each check node, send a message containing a new estimate
     * to each of its neighbouring variable node.
     */
    #pragma omp parallel for num_threads(4)
    for(size_t cn_idx = firstCnVectorIdx; cn_idx < lastCnVectorIdx; cn_idx+=16) {
        __m128i vsign_global = _mm_set1_epi8(0);
        __m128i vmin_llr1 = _mm_set1_epi8(vSAT_POS_MSG);
        __m128i vmin_llr2 = _mm_set1_epi8(vSAT_POS_MSG);

        __m128i vabs[DEG_1];

        char tmpVn[cn_deg_max][16];
        __m128i vvn[DEG_1];

        /* We compute a different estimate (the message sent), for each
         * neighbouring VN */
        for (size_t edge_idx = 0 ; edge_idx < DEG_1 ; edge_idx++)
        {
            for (size_t i = 0 ; i < 16 ; i++)
            {
                int cn_offset = (cn_idx + i) * cn_deg_max;
                tmpVn[edge_idx][i] = *floodedDecoder->p_vn_addr[cn_offset + edge_idx];
            }

            vvn[edge_idx] = _mm_loadu_si128((__m128i*)tmpVn[edge_idx]);

            vsign_global = _mm_xor_si128(vsign_global, vvn[edge_idx]);

            vabs[edge_idx] = _mm_abs_epi8(vvn[edge_idx]);

            __m128i vmax_abs_min_llr1 = _mm_max_epi8(vabs[edge_idx], vmin_llr1);

            __m128i vcomp = _mm_cmpgt_epi8(vabs[edge_idx], vmin_llr2);
            vmin_llr2 = _mm_and_si128(vcomp, vmin_llr2);
            vmin_llr2 = _mm_or_si128(vmin_llr2, _mm_andnot_si128(vcomp, vmax_abs_min_llr1));

            vmin_llr1 = _mm_min_epi8(vmin_llr1, vabs[edge_idx]);
        }

#if (DEG_1 & 0x1)
        const __m128i vone = _mm_set1_epi8(0xff);
        vsign_global = _mm_xor_si128(vone, vsign_global);
#endif

        for(size_t edge_idx = 0; edge_idx < DEG_1; edge_idx++) {
            __m128i vsign = _mm_xor_si128(vvn[edge_idx], vsign_global);

            __m128i vcomp = _mm_cmpeq_epi8(vabs[edge_idx], vmin_llr1);
            __m128i vmin = _mm_and_si128(vcomp, vmin_llr2);
            vmin = _mm_or_si128(vmin, _mm_andnot_si128(vcomp, vmin_llr1));
            __m128i vmin_neg = _mm_sub_epi8(vzero, vmin);

            vcomp = _mm_cmpgt_epi8(vzero, vsign);
            __m128i vcn_msg = _mm_and_si128(vcomp, vmin_neg);
            vcn_msg = _mm_or_si128(vcn_msg, _mm_andnot_si128(vcomp, vmin));

            char cn_msgs_tmp[16];
            _mm_storeu_si128((__m128i*)cn_msgs_tmp, vcn_msg);

            for (int i = 0 ; i < 16 ; i++) {
                int cn_offset = (cn_idx+i)*cn_deg_max + edge_idx;
                floodedDecoder->cn_msgs[cn_offset] = cn_msgs_tmp[i];
            }
        }
    }

    /* Leftovers */
    for(size_t cn_idx = lastCnVectorIdx; cn_idx < lastCnIdx; cn_idx++) {

        int cn_offset = cn_idx*cn_deg_max;

        char signGlobal = 0;
        char minLLR1 = vSAT_POS_MSG;
        char minLLR2 = vSAT_POS_MSG;

        char abs[DEG_1];

        /* We compute a different estimate (the message sent), for each
         * neighbouring VN */
        for(size_t edge_idx = 0; edge_idx < DEG_1; edge_idx++) {
            char vn = *floodedDecoder->p_vn_addr[cn_offset + edge_idx];
            signGlobal ^= vn;
            abs[edge_idx] = (vn >= 0 ? vn : -vn);
            char maxAbsMinLLR1 = abs[edge_idx] > minLLR1 ? abs[edge_idx] : minLLR1;
            minLLR2 = minLLR2 < abs[edge_idx] ? minLLR2 : maxAbsMinLLR1;
            minLLR1 = minLLR1 < abs[edge_idx] ? minLLR1 : abs[edge_idx];
        }

#if (DEG_1 & 0x1)
        signGlobal = ~signGlobal;
#endif

        for(size_t edge_idx = 0; edge_idx < DEG_1; edge_idx++) {
            char sign = *floodedDecoder->p_vn_addr[cn_offset + edge_idx] ^ signGlobal;
            char min = abs[edge_idx] == minLLR1 ? minLLR2 : minLLR1;
            floodedDecoder->cn_msgs[cn_offset + edge_idx] = sign < 0 ? -min : min;
        }
    }
}

static inline void computeCnMsgsDeg2(REDS_DVB_DECODER_FloodedDecoder_t* floodedDecoder)
{
    /* SSE constants */
    const __m128i vzero = _mm_set1_epi8(0);

    /* Loop constants */
    const size_t firstCnVectorIdx = DEG_1_COMPUTATIONS;
    const size_t lastCnVectorIdx = DEG_1_COMPUTATIONS + (((DEG_2_COMPUTATIONS)/16) * 16);
    const size_t lastCnIdx = DEG_1_COMPUTATIONS + DEG_2_COMPUTATIONS;

    #pragma omp parallel for num_threads(4)
    for(size_t cn_idx = firstCnVectorIdx; cn_idx < lastCnVectorIdx; cn_idx++) {

        __m128i vsign_global = _mm_set1_epi8(0);
        __m128i vmin_llr1 = _mm_set1_epi8(vSAT_POS_MSG);
        __m128i vmin_llr2 = _mm_set1_epi8(vSAT_POS_MSG);

        __m128i vabs[DEG_2];

        char tmpVn[cn_deg_max][16];
        __m128i vvn[DEG_2];

        for (size_t i = 0 ; i < 16 ; i++)
        {
            int cn_offset = (cn_idx + i) * cn_deg_max;
            for (size_t edge_idx = 0 ; edge_idx < DEG_2 ; edge_idx++)
            {
                tmpVn[edge_idx][i] = *floodedDecoder->p_vn_addr[cn_offset + edge_idx];
            }
        }

        for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
            vvn[edge_idx] = _mm_loadu_si128((__m128i*)tmpVn[edge_idx]);
        }

        /* We compute a different estimate (the message sent), for each
         * neighbouring VN */
        for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
            vsign_global = _mm_xor_si128(vsign_global, vvn[edge_idx]);

            vabs[edge_idx] = _mm_abs_epi8(vvn[edge_idx]);

            __m128i vmax_abs_min_llr1 = _mm_max_epi8(vabs[edge_idx], vmin_llr1);

            __m128i vcomp = _mm_cmpgt_epi8(vabs[edge_idx], vmin_llr2);
            vmin_llr2 = _mm_and_si128(vcomp, vmin_llr2);
            vmin_llr2 = _mm_or_si128(vmin_llr2, _mm_andnot_si128(vcomp, vmax_abs_min_llr1));

            vmin_llr1 = _mm_min_epi8(vmin_llr1, vabs[edge_idx]);
        }

#if (DEG_2 & 0x1)
        const __m128i vone = _mm_set1_epi8(0xff);
        vsign_global = _mm_xor_si128(vone, vsign_global);
#endif

        for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
            __m128i vsign = _mm_xor_si128(vvn[edge_idx], vsign_global);

            __m128i vcomp = _mm_cmpeq_epi8(vabs[edge_idx], vmin_llr1);
            __m128i vmin = _mm_and_si128(vcomp, vmin_llr2);
            vmin = _mm_or_si128(vmin, _mm_andnot_si128(vcomp, vmin_llr1));
            __m128i vmin_neg = _mm_sub_epi8(vzero, vmin);

            vcomp = _mm_cmpgt_epi8(vzero, vsign);
            __m128i vcn_msg = _mm_and_si128(vcomp, vmin_neg);
            vcn_msg = _mm_or_si128(vcn_msg, _mm_andnot_si128(vcomp, vmin));

            char cn_msgs_tmp[16];
            _mm_storeu_si128((__m128i*)cn_msgs_tmp, vcn_msg);

            for (int i = 0 ; i < 16 ; i++) {
                int cn_offset = (cn_idx+i)*cn_deg_max + edge_idx;
                floodedDecoder->cn_msgs[cn_offset] = cn_msgs_tmp[i];
            }
        }
    }

    /* Leftovers */
    for(size_t cn_idx = lastCnVectorIdx; cn_idx < lastCnIdx; cn_idx++) {

        int cn_offset = cn_idx*cn_deg_max;

        char signGlobal = 0;
        char minLLR1 = vSAT_POS_MSG;
        char minLLR2 = vSAT_POS_MSG;

        char abs[DEG_2];

        /* We compute a different estimate (the message sent), for each
         * neighbouring VN */
        for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
            char vn = *floodedDecoder->p_vn_addr[cn_offset + edge_idx];
            signGlobal ^= vn;
            abs[edge_idx] = (vn >= 0 ? vn : -vn);
            char maxAbsMinLLR1 = abs[edge_idx] > minLLR1 ? abs[edge_idx] : minLLR1;
            minLLR2 = minLLR2 < abs[edge_idx] ? minLLR2 : maxAbsMinLLR1;
            minLLR1 = minLLR1 < abs[edge_idx] ? minLLR1 : abs[edge_idx];
        }

#if (DEG_2 & 0x1)
        signGlobal = ~signGlobal;
#endif

        for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
            char sign = *floodedDecoder->p_vn_addr[cn_offset + edge_idx] ^ signGlobal;
            char min = abs[edge_idx] == minLLR1 ? minLLR2 : minLLR1;
            floodedDecoder->cn_msgs[cn_offset + edge_idx] = sign < 0 ? -min : min;
        }
    }
}

void DECODE_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder,
                                           char var_nodes[],
                                           char Rprime_fix[],
                                           int nombre_iterations)
{
    REDS_DVB_DECODER_FloodedDecoder_t* floodedDecoder = (REDS_DVB_DECODER_FloodedDecoder_t*)decoder->decoder;
    /* Initialize variable node messages */
    for(size_t i = 0; i < nb_vn; i++) {
        for(size_t j = 0; j < vn_deg_max; j++) {
            floodedDecoder->vn_msgs[i*vn_deg_max + j] = var_nodes[i];
        }
    }

    /* Loop a fixed number of iterations */
    while(nombre_iterations--) {

        computeCnMsgsDeg1(floodedDecoder);

#if (NB_DEGRES <= 2)
        computeCnMsgsDeg2(floodedDecoder);
#else
#error "Code should be adapted to handle more degrees"
#endif
        /* Bit node updates.
         * For each variable node, compute two things :
         * 1) A new message to be sent to each of its neighbouring CNs
         * 2) A new estimate to be kept to itself
         */
        #pragma omp parallel for num_threads(4)
        for(size_t vn_idx = 0; vn_idx < nb_vn; vn_idx++) {
            int estimate_acc = var_nodes[vn_idx];
            size_t vn_offset = vn_idx*vn_deg_max;
            const size_t cn_addrSize = floodedDecoder->p_cn_addrSize[vn_idx];

            for(size_t edge_idx = 0; edge_idx < cn_addrSize; edge_idx++)
            {
                estimate_acc += *floodedDecoder->p_cn_addr[vn_offset + edge_idx];
            }

            if(estimate_acc > vSAT_POS_VAR) floodedDecoder->vn_est[vn_idx] = vSAT_POS_VAR;
            else if(estimate_acc < vSAT_NEG_VAR) floodedDecoder->vn_est[vn_idx] = vSAT_NEG_VAR;
            else floodedDecoder->vn_est[vn_idx] = (char)estimate_acc;

            for(size_t edge_idx = 0; edge_idx < cn_addrSize; edge_idx++)
            {
                int msg = *floodedDecoder->p_cn_addr[vn_offset + edge_idx];
                int vn_msg = estimate_acc - msg;

                if(vn_msg > vSAT_POS_VAR) vn_msg = vSAT_POS_VAR;
                else if(vn_msg < vSAT_NEG_VAR) vn_msg = vSAT_NEG_VAR;

                floodedDecoder->vn_msgs[vn_offset + edge_idx] = (char)vn_msg;
            }
        }
    }

    /* Hard decision */
    for(size_t vn_idx = 0; vn_idx < nb_vn; vn_idx++) {
        Rprime_fix[vn_idx] = floodedDecoder->vn_est[vn_idx] > 0 ? 1 : 0;
    }
}
