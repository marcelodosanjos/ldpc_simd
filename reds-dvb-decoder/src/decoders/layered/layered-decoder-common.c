/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of reds-dvb-decoder.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "layered-decoder-common.h"

#include <stdint.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include <tmmintrin.h>

#include "reds-dvb-decoder/reds-dvb-decoder.h"

#ifndef CODERATE_SUFFIX
#error "Error: CODERATE_SUFFIX is not defined"
#endif /* CODERATE_SUFFIX */

#define INIT_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_layered_ ## x ## _Init
#define INIT_FUNCTION_NAME(x) INIT_FUNCTION_NAME_HELPER(x)

#define TERMINATE_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_layered_ ## x ## _Terminate
#define TERMINATE_FUNCTION_NAME(x) TERMINATE_FUNCTION_NAME_HELPER(x)

#define DECODE_FUNCTION_NAME_HELPER(x) REDS_DVB_DECODER_layered_ ## x ## _Decode
#define DECODE_FUNCTION_NAME(x) DECODE_FUNCTION_NAME_HELPER(x)

#define SSE_16BIT_ELEM      8
#define INT16_WIDTH 16

/* TODO JKI check that DEG_1 > DEG_2 */
static const unsigned int cn_deg_max = DEG_1;

static inline void cn_kernel30(
    size_t cn_idx,
    int16_t **p_vn_addr,
    int16_t *cv_msgs)
{
    const size_t cn_deg = 30;
    const size_t max_vec_elem = SSE_16BIT_ELEM*((cn_deg/SSE_16BIT_ELEM) + 1);

    __m128i vc_msgs0_vec, vc_msgs1_vec, vc_msgs2_vec, vc_msgs3_vec;
    __m128i abs0_vec, abs1_vec, abs2_vec, abs3_vec;
    __m128i min1_res0_vec, min1_res1_vec, min1_res2_vec, min1_res3_vec;

    __m128i pos_mask_vec;
    __m128i pos_brdcst_vec;
    __m128i min1_brdcst_vec;
    __m128i min2_brdcst_vec;

    __m128i final0_min_vec;
    __m128i final1_min_vec;
    __m128i final2_min_vec;
    __m128i final3_min_vec;

    __m128i sign0_vec;
    __m128i sign1_vec;
    __m128i sign2_vec;
    __m128i sign3_vec;

    __m128i new_msgs0_vec;
    __m128i new_msgs1_vec;
    __m128i new_msgs2_vec;
    __m128i new_msgs3_vec;

    int16_t v_to_c_msgs[max_vec_elem];
    int16_t new_msgs[max_vec_elem];
    int16_t global_sign;

    int8_t min1_pos0;
    int8_t min1_pos1;
    int8_t min1_pos2;
    int8_t min1_pos3;

    int16_t min1_res0 = INT16_MAX;
    int16_t min1_res1 = INT16_MAX;
    int16_t min1_res2 = INT16_MAX;
    int16_t min1_res3 = INT16_MAX;

    int16_t min2 = INT16_MAX;
    int16_t new_msg;
    int16_t s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14;
    size_t cn_offset;

    cn_offset = cn_idx*cn_deg_max;

    /* Collect all messages coming from VNs adjacent to current CN */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        v_to_c_msgs[vn_idx] =
            *p_vn_addr[cn_offset+vn_idx] -
            cv_msgs[cn_offset+vn_idx];
    }

    for(size_t i = cn_deg; i < max_vec_elem; i++) {
        v_to_c_msgs[i] = INT16_MAX;
    }

    /* Compute new estimation directed to VNs,
     * store it for the next iteration, and update estimation on VNs. */

    /* Compute global sign. It is updated later for each individual VN we send a message */
    s0 = v_to_c_msgs[0] ^ v_to_c_msgs[1];
    s1 = v_to_c_msgs[2] ^ v_to_c_msgs[3];
    s2 = v_to_c_msgs[4] ^ v_to_c_msgs[5];
    s3 = v_to_c_msgs[6] ^ v_to_c_msgs[7];
    s4 = v_to_c_msgs[8] ^ v_to_c_msgs[9];
    s5 = v_to_c_msgs[10] ^ v_to_c_msgs[11];
    s6 = v_to_c_msgs[12] ^ v_to_c_msgs[13];
    s7 = v_to_c_msgs[14] ^ v_to_c_msgs[15];
    s8 = v_to_c_msgs[16] ^ v_to_c_msgs[17];
    s9 = v_to_c_msgs[18] ^ v_to_c_msgs[19];
    s10 = v_to_c_msgs[20] ^ v_to_c_msgs[21];
    s11 = v_to_c_msgs[22] ^ v_to_c_msgs[23];
    s12 = v_to_c_msgs[24] ^ v_to_c_msgs[25];
    s13 = v_to_c_msgs[26] ^ v_to_c_msgs[27];
    s14 = v_to_c_msgs[28] ^ v_to_c_msgs[29];

    s0 = s0 ^ s1;
    s2 = s2 ^ s3;
    s4 = s4 ^ s5;
    s6 = s6 ^ s7;
    s8 = s8 ^ s9;
    s10 = s10 ^ s11;
    s12 = s12 ^ s13;

    s0 = s0 ^ s2;
    s4 = s4 ^ s6;
    s8 = s8 ^ s10;

    s0 = s0 ^ s4;
    s8 = s8 ^ s12;

    global_sign = s0 ^ s8 ^ s14;

    /* Compute the first minimum among all fetched VN messages */
    vc_msgs0_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 0);
    vc_msgs1_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 1);
    vc_msgs2_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 2);
    vc_msgs3_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 3);
    abs0_vec = _mm_abs_epi16(vc_msgs0_vec);
    abs1_vec = _mm_abs_epi16(vc_msgs1_vec);
    abs2_vec = _mm_abs_epi16(vc_msgs2_vec);
    abs3_vec = _mm_abs_epi16(vc_msgs3_vec);
    min1_res0_vec = _mm_minpos_epu16(abs0_vec);
    min1_res1_vec = _mm_minpos_epu16(abs1_vec);
    min1_res2_vec = _mm_minpos_epu16(abs2_vec);
    min1_res3_vec = _mm_minpos_epu16(abs3_vec);
    min1_res0 = _mm_extract_epi16(min1_res0_vec, 0);
    min1_res1 = _mm_extract_epi16(min1_res1_vec, 0);
    min1_res2 = _mm_extract_epi16(min1_res2_vec, 0);
    min1_res3 = _mm_extract_epi16(min1_res3_vec, 0);
    min1_pos0 = _mm_extract_epi8(min1_res0_vec, 2);
    min1_pos1 = _mm_extract_epi8(min1_res1_vec, 2);
    min1_pos2 = _mm_extract_epi8(min1_res2_vec, 2);
    min1_pos3 = _mm_extract_epi8(min1_res3_vec, 2);

    /* Compute the second minimum among all fetched VN messages. */
    __m128i min1_global_vec = _mm_set_epi16(0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res0, 0);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res1, 1);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res2, 2);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res3, 3);
    __m128i min1_global_res_vec = _mm_minpos_epu16(min1_global_vec);
    int8_t min1_global_pos = _mm_extract_epi8(min1_global_res_vec, 2);
    int16_t min1_global_res = _mm_extract_epi16(min1_global_res_vec, 0);

    pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
    pos_brdcst_vec = _mm_set1_epi16(min1_global_pos);
    pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);
    min1_global_vec = _mm_or_si128(min1_global_vec, pos_mask_vec);
    min2 = _mm_extract_epi16(_mm_minpos_epu16(min1_global_vec), 0);

    if(cn_deg & 0x1 ) global_sign = ~global_sign;

    /* Broadcast the first minimum and second minimum*/
    min1_brdcst_vec = _mm_set1_epi16(min1_global_res);
    min2_brdcst_vec = _mm_set1_epi16(min2);

    /* Fusion min1 vector and min2 such that min2 is placed at the position where
     * we found min1 */
    final0_min_vec = min1_brdcst_vec;
    final1_min_vec = min1_brdcst_vec;
    final2_min_vec = min1_brdcst_vec;
    final3_min_vec = min1_brdcst_vec;

    if(min1_global_pos == 0) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos0);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final0_min_vec = _mm_andnot_si128(pos_mask_vec, final0_min_vec);
        final0_min_vec = _mm_or_si128(final0_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 1) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos1);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final1_min_vec = _mm_andnot_si128(pos_mask_vec, final1_min_vec);
        final1_min_vec = _mm_or_si128(final1_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 2) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos2);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final2_min_vec = _mm_andnot_si128(pos_mask_vec, final2_min_vec);
        final2_min_vec = _mm_or_si128(final2_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 3) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos3);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final3_min_vec = _mm_andnot_si128(pos_mask_vec, final3_min_vec);
        final3_min_vec = _mm_or_si128(final3_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }

    /* Send a different message to each neighbouring VN */
    sign0_vec = _mm_xor_si128(vc_msgs0_vec, _mm_set1_epi16(global_sign));
    sign1_vec = _mm_xor_si128(vc_msgs1_vec, _mm_set1_epi16(global_sign));
    sign2_vec = _mm_xor_si128(vc_msgs2_vec, _mm_set1_epi16(global_sign));
    sign3_vec = _mm_xor_si128(vc_msgs3_vec, _mm_set1_epi16(global_sign));
    sign0_vec = _mm_srai_epi16(sign0_vec, 15);
    sign1_vec = _mm_srai_epi16(sign1_vec, 15);
    sign2_vec = _mm_srai_epi16(sign2_vec, 15);
    sign3_vec = _mm_srai_epi16(sign3_vec, 15);
    sign0_vec = _mm_or_si128(sign0_vec, _mm_set1_epi16(1));
    sign1_vec = _mm_or_si128(sign1_vec, _mm_set1_epi16(1));
    sign2_vec = _mm_or_si128(sign2_vec, _mm_set1_epi16(1));
    sign3_vec = _mm_or_si128(sign3_vec, _mm_set1_epi16(1));
    new_msgs0_vec = _mm_mullo_epi16(sign0_vec, final0_min_vec);
    new_msgs1_vec = _mm_mullo_epi16(sign1_vec, final1_min_vec);
    new_msgs2_vec = _mm_mullo_epi16(sign2_vec, final2_min_vec);
    new_msgs3_vec = _mm_mullo_epi16(sign3_vec, final3_min_vec);
    _mm_storeu_si128((__m128i*)new_msgs+0, new_msgs0_vec);
    _mm_storeu_si128((__m128i*)new_msgs+1, new_msgs1_vec);
    _mm_storeu_si128((__m128i*)new_msgs+2, new_msgs2_vec);
    _mm_storeu_si128((__m128i*)new_msgs+3, new_msgs3_vec);

    /* Store new messages back to VNs */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        new_msg = new_msgs[vn_idx];
        cv_msgs[cn_offset+vn_idx] = new_msg;
        *p_vn_addr[cn_offset+vn_idx] = new_msg + v_to_c_msgs[vn_idx];
    }
}

static inline void cn_kernel27(
    size_t cn_idx,
    int16_t **p_vn_addr,
    int16_t *cv_msgs)
{
    const size_t cn_deg = 27;
    const size_t max_vec_elem = SSE_16BIT_ELEM*((cn_deg/SSE_16BIT_ELEM) + 1);

    __m128i vc_msgs0_vec, vc_msgs1_vec, vc_msgs2_vec, vc_msgs3_vec;
    __m128i abs0_vec, abs1_vec, abs2_vec, abs3_vec;
    __m128i min1_res0_vec, min1_res1_vec, min1_res2_vec, min1_res3_vec;

    __m128i pos_mask_vec;
    __m128i pos_brdcst_vec;
    __m128i min1_brdcst_vec;
    __m128i min2_brdcst_vec;

    __m128i final0_min_vec;
    __m128i final1_min_vec;
    __m128i final2_min_vec;
    __m128i final3_min_vec;

    __m128i sign0_vec;
    __m128i sign1_vec;
    __m128i sign2_vec;
    __m128i sign3_vec;

    __m128i new_msgs0_vec;
    __m128i new_msgs1_vec;
    __m128i new_msgs2_vec;
    __m128i new_msgs3_vec;

    int16_t v_to_c_msgs[max_vec_elem];
    int16_t new_msgs[max_vec_elem];
    int16_t global_sign;

    int8_t min1_pos0;
    int8_t min1_pos1;
    int8_t min1_pos2;
    int8_t min1_pos3;

    int16_t min1_res0 = INT16_MAX;
    int16_t min1_res1 = INT16_MAX;
    int16_t min1_res2 = INT16_MAX;
    int16_t min1_res3 = INT16_MAX;

    int16_t min2 = INT16_MAX;
    int16_t new_msg;
    int16_t s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13;
    size_t cn_offset;

    cn_offset = cn_idx*cn_deg_max;

    /* Collect all messages coming from VNs adjacent to current CN */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        v_to_c_msgs[vn_idx] =
            *p_vn_addr[cn_offset+vn_idx] -
            cv_msgs[cn_offset+vn_idx];
    }

    for(size_t i = cn_deg; i < max_vec_elem; i++) {
        v_to_c_msgs[i] = INT16_MAX;
    }

    /* Compute new estimation directed to VNs,
     * store it for the next iteration, and update estimation on VNs. */

    /* Compute global sign. It is updated later for each individual VN we send a message */
    s0 = v_to_c_msgs[0] ^ v_to_c_msgs[1];
    s1 = v_to_c_msgs[2] ^ v_to_c_msgs[3];
    s2 = v_to_c_msgs[4] ^ v_to_c_msgs[5];
    s3 = v_to_c_msgs[6] ^ v_to_c_msgs[7];
    s4 = v_to_c_msgs[8] ^ v_to_c_msgs[9];
    s5 = v_to_c_msgs[10] ^ v_to_c_msgs[11];
    s6 = v_to_c_msgs[12] ^ v_to_c_msgs[13];
    s7 = v_to_c_msgs[14] ^ v_to_c_msgs[15];
    s8 = v_to_c_msgs[16] ^ v_to_c_msgs[17];
    s9 = v_to_c_msgs[18] ^ v_to_c_msgs[19];
    s10 = v_to_c_msgs[20] ^ v_to_c_msgs[21];
    s11 = v_to_c_msgs[22] ^ v_to_c_msgs[23];
    s12 = v_to_c_msgs[24] ^ v_to_c_msgs[25];
    s13 = v_to_c_msgs[26];

    s0 = s0 ^ s1;
    s2 = s2 ^ s3;
    s4 = s4 ^ s5;
    s6 = s6 ^ s7;
    s8 = s8 ^ s9;
    s10 = s10 ^ s11;
    s12 = s12 ^ s13;

    s0 = s0 ^ s2;
    s4 = s4 ^ s6;
    s8 = s8 ^ s10;

    s0 = s0 ^ s4;
    s8 = s8 ^ s12;

    global_sign = s0 ^ s8;

    /* Compute the first minimum among all fetched VN messages */
    vc_msgs0_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 0);
    vc_msgs1_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 1);
    vc_msgs2_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 2);
    vc_msgs3_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs + 3);
    abs0_vec = _mm_abs_epi16(vc_msgs0_vec);
    abs1_vec = _mm_abs_epi16(vc_msgs1_vec);
    abs2_vec = _mm_abs_epi16(vc_msgs2_vec);
    abs3_vec = _mm_abs_epi16(vc_msgs3_vec);
    min1_res0_vec = _mm_minpos_epu16(abs0_vec);
    min1_res1_vec = _mm_minpos_epu16(abs1_vec);
    min1_res2_vec = _mm_minpos_epu16(abs2_vec);
    min1_res3_vec = _mm_minpos_epu16(abs3_vec);
    min1_res0 = _mm_extract_epi16(min1_res0_vec, 0);
    min1_res1 = _mm_extract_epi16(min1_res1_vec, 0);
    min1_res2 = _mm_extract_epi16(min1_res2_vec, 0);
    min1_res3 = _mm_extract_epi16(min1_res3_vec, 0);
    min1_pos0 = _mm_extract_epi8(min1_res0_vec, 2);
    min1_pos1 = _mm_extract_epi8(min1_res1_vec, 2);
    min1_pos2 = _mm_extract_epi8(min1_res2_vec, 2);
    min1_pos3 = _mm_extract_epi8(min1_res3_vec, 2);

    /* Compute the second minimum among all fetched VN messages. */
    __m128i min1_global_vec = _mm_set_epi16(0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res0, 0);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res1, 1);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res2, 2);
    min1_global_vec = _mm_insert_epi16(min1_global_vec, min1_res3, 3);
    __m128i min1_global_res_vec = _mm_minpos_epu16(min1_global_vec);
    int8_t min1_global_pos = _mm_extract_epi8(min1_global_res_vec, 2);
    int16_t min1_global_res = _mm_extract_epi16(min1_global_res_vec, 0);

    pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
    pos_brdcst_vec = _mm_set1_epi16(min1_global_pos);
    pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);
    min1_global_vec = _mm_or_si128(min1_global_vec, pos_mask_vec);
    min2 = _mm_extract_epi16(_mm_minpos_epu16(min1_global_vec), 0);

    if(cn_deg & 0x1 ) global_sign = ~global_sign;

    /* Broadcast the first minimum and second minimum*/
    min1_brdcst_vec = _mm_set1_epi16(min1_global_res);
    min2_brdcst_vec = _mm_set1_epi16(min2);

    /* Fusion min1 vector and min2 such that min2 is placed at the position where
     * we found min1 */
    final0_min_vec = min1_brdcst_vec;
    final1_min_vec = min1_brdcst_vec;
    final2_min_vec = min1_brdcst_vec;
    final3_min_vec = min1_brdcst_vec;

    if(min1_global_pos == 0) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos0);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final0_min_vec = _mm_andnot_si128(pos_mask_vec, final0_min_vec);
        final0_min_vec = _mm_or_si128(final0_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 1) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos1);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final1_min_vec = _mm_andnot_si128(pos_mask_vec, final1_min_vec);
        final1_min_vec = _mm_or_si128(final1_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 2) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos2);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final2_min_vec = _mm_andnot_si128(pos_mask_vec, final2_min_vec);
        final2_min_vec = _mm_or_si128(final2_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }
    else if(min1_global_pos == 3) {
        pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
        pos_brdcst_vec = _mm_set1_epi16(min1_pos3);
        pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);

        final3_min_vec = _mm_andnot_si128(pos_mask_vec, final3_min_vec);
        final3_min_vec = _mm_or_si128(final3_min_vec, _mm_and_si128(pos_mask_vec, min2_brdcst_vec));
    }

    /* Send a different message to each neighbouring VN */
    sign0_vec = _mm_xor_si128(vc_msgs0_vec, _mm_set1_epi16(global_sign));
    sign1_vec = _mm_xor_si128(vc_msgs1_vec, _mm_set1_epi16(global_sign));
    sign2_vec = _mm_xor_si128(vc_msgs2_vec, _mm_set1_epi16(global_sign));
    sign3_vec = _mm_xor_si128(vc_msgs3_vec, _mm_set1_epi16(global_sign));
    sign0_vec = _mm_srai_epi16(sign0_vec, 15);
    sign1_vec = _mm_srai_epi16(sign1_vec, 15);
    sign2_vec = _mm_srai_epi16(sign2_vec, 15);
    sign3_vec = _mm_srai_epi16(sign3_vec, 15);
    sign0_vec = _mm_or_si128(sign0_vec, _mm_set1_epi16(1));
    sign1_vec = _mm_or_si128(sign1_vec, _mm_set1_epi16(1));
    sign2_vec = _mm_or_si128(sign2_vec, _mm_set1_epi16(1));
    sign3_vec = _mm_or_si128(sign3_vec, _mm_set1_epi16(1));
    new_msgs0_vec = _mm_mullo_epi16(sign0_vec, final0_min_vec);
    new_msgs1_vec = _mm_mullo_epi16(sign1_vec, final1_min_vec);
    new_msgs2_vec = _mm_mullo_epi16(sign2_vec, final2_min_vec);
    new_msgs3_vec = _mm_mullo_epi16(sign3_vec, final3_min_vec);
    _mm_storeu_si128((__m128i*)new_msgs+0, new_msgs0_vec);
    _mm_storeu_si128((__m128i*)new_msgs+1, new_msgs1_vec);
    _mm_storeu_si128((__m128i*)new_msgs+2, new_msgs2_vec);
    _mm_storeu_si128((__m128i*)new_msgs+3, new_msgs3_vec);

    /* Store new messages back to VNs */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        new_msg = new_msgs[vn_idx];
        cv_msgs[cn_offset+vn_idx] = new_msg;
        *p_vn_addr[cn_offset+vn_idx] = new_msg + v_to_c_msgs[vn_idx];
    }
}

static inline void cn_kernel7(
    size_t cn_idx,
    int16_t **p_vn_addr,
    int16_t *cv_msgs)
{
    const size_t cn_deg = 7;

    __m128i vc_msgs_vec;
    __m128i abs_vec;
    __m128i min1_res_vec;
    __m128i pos_mask_vec;
    __m128i pos_brdcst_vec;
    __m128i min1_brdcst_vec;
    __m128i min2_brdcst_vec;
    __m128i final_min_vec;
    __m128i sign_vec;
    __m128i new_msgs_vec;

    int8_t min1_pos;
    int16_t v_to_c_msgs[SSE_16BIT_ELEM];
    int16_t new_msgs[SSE_16BIT_ELEM];
    int16_t global_sign;
    int16_t min1 = INT16_MAX;
    int16_t min2 = INT16_MAX;
    int16_t new_msg;
    int16_t s0, s1, s2, s3;
    size_t cn_offset;

    cn_offset = cn_idx*cn_deg_max;

    /* Collect all messages coming from VNs adjacent to current CN */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        v_to_c_msgs[vn_idx] =
            *p_vn_addr[cn_offset+vn_idx] -
            cv_msgs[cn_offset+vn_idx];
    }

    for(size_t i = cn_deg; i < SSE_16BIT_ELEM; i++) {
        v_to_c_msgs[i] = INT16_MAX;
    }

    /* Compute new estimation directed to VNs,
     * store it for the next iteration, and update estimation on VNs. */

    /* Compute global sign. It is updated later for each individual VN we send a message */
    s0 = v_to_c_msgs[0] ^ v_to_c_msgs[1];
    s1 = v_to_c_msgs[2] ^ v_to_c_msgs[3];
    s2 = v_to_c_msgs[4] ^ v_to_c_msgs[5];
    s3 = v_to_c_msgs[6];

    s0 = s0 ^ s1;
    s2 = s2 ^ s3;

    global_sign = s0 ^ s2;

    /* Compute the first minimum among all fetched VN messages */
    vc_msgs_vec = _mm_loadu_si128((__m128i*)v_to_c_msgs);
    abs_vec = _mm_abs_epi16(vc_msgs_vec);
    min1_res_vec = _mm_minpos_epu16(abs_vec);
    min1 = _mm_extract_epi16(min1_res_vec, 0);
    min1_pos = _mm_extract_epi8(min1_res_vec, 2);

    /* Compute the second minimum among all fetched VN messages.
     * For that, the first minimum found is replaced by a MAX value */
    pos_mask_vec = _mm_set_epi16(7,6,5,4,3,2,1,0);
    pos_brdcst_vec = _mm_set1_epi16(min1_pos);
    pos_mask_vec = _mm_cmpeq_epi16(pos_mask_vec, pos_brdcst_vec);
    abs_vec = _mm_or_si128(abs_vec, pos_mask_vec);
    min2 = _mm_extract_epi16(_mm_minpos_epu16(abs_vec), 0);

    /* Invert sign because CN degree is odd */
    global_sign = ~global_sign;

    /* Broadcast the first minimum and second minimum*/
    min1_brdcst_vec = _mm_set1_epi16(min1);
    min2_brdcst_vec = _mm_set1_epi16(min2);

    /* Fusion min1 vector and min2 such that min2 is placed at the position where
     * we found min1 */
    min1_brdcst_vec = _mm_andnot_si128(pos_mask_vec, min1_brdcst_vec);
    min2_brdcst_vec = _mm_and_si128(pos_mask_vec, min2_brdcst_vec);

    final_min_vec = _mm_or_si128(min1_brdcst_vec, min2_brdcst_vec);

    /* Send a different message to each neighbouring VN */
    sign_vec = _mm_xor_si128(vc_msgs_vec, _mm_set1_epi16(global_sign));
    sign_vec = _mm_srai_epi16(sign_vec, 15);
    sign_vec = _mm_or_si128(sign_vec, _mm_set1_epi16(1));
    new_msgs_vec = _mm_mullo_epi16(sign_vec, final_min_vec);
    _mm_storeu_si128((__m128i*)new_msgs, new_msgs_vec);


    /* Store new messages back to VNs */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        new_msg = new_msgs[vn_idx];
        cv_msgs[cn_offset+vn_idx] = new_msg;
        *p_vn_addr[cn_offset+vn_idx] = new_msg + v_to_c_msgs[vn_idx];
    }
}

static inline void generic_cn_kernel(
    size_t cn_idx,
    const size_t cn_deg,
    const size_t cn_deg_max,
    int16_t **p_vn_addr,
    int16_t *cv_msgs)
{
    int16_t v_to_c_msgs[cn_deg_max];
    int16_t abs_msgs[cn_deg_max];
    size_t cn_offset;
    int16_t global_sign, sign;
    int16_t min1;
    int16_t min2;
    int16_t min;
    int16_t abs_msg, msg;
    int16_t new_msg;
    int16_t abs_mask;

    cn_offset = cn_idx*cn_deg_max;

    /* Collect all messages coming from VNs adjacent to current CN */
    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        v_to_c_msgs[vn_idx] =
            *p_vn_addr[cn_offset+vn_idx] -
            cv_msgs[cn_offset+vn_idx];
    }

    /* Compute new estimation directed to VNs,
     * store it for the next iteration, and update estimation on VNs. */
    global_sign = 0;
    min1 = INT16_MAX;
    min2 = INT16_MAX;

    if(cn_deg & 0x1) global_sign = ~global_sign;

    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        msg = v_to_c_msgs[vn_idx];
        global_sign ^= msg;
        abs_mask = msg >> (INT16_WIDTH - 1);
        abs_msg = (msg + abs_mask) ^ abs_mask;
        min2 = min2 > abs_msg ? (min1 > abs_msg ? min1 : abs_msg) : min2;
        min1 = min1 > abs_msg ? abs_msg:min1;
        abs_msgs[vn_idx] = abs_msg;
    }

    for(size_t vn_idx = 0; vn_idx < cn_deg; vn_idx++) {
        min = abs_msgs[vn_idx] != min1 ? min1:min2;
        sign = (global_sign ^ v_to_c_msgs[vn_idx]);
        sign = 1 | (sign >> (INT16_WIDTH-1)); // either -1 or 1
        new_msg = sign * min;
        cv_msgs[cn_offset+vn_idx] = new_msg;
        *p_vn_addr[cn_offset+vn_idx] = new_msg + v_to_c_msgs[vn_idx];
    }
}



REDS_DVB_DECODER_Error_t INIT_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder)
{
    REDS_DVB_DECODER_LayeredDecoder_t* layeredDecoder = malloc(sizeof(REDS_DVB_DECODER_LayeredDecoder_t));
    decoder->decoder = layeredDecoder;

    layeredDecoder->p_vn_addr = malloc(sizeof(*layeredDecoder->p_vn_addr) * MESSAGE);
    layeredDecoder->var_nodes = malloc(sizeof(*layeredDecoder->var_nodes) * NOEUD);
    layeredDecoder->c_to_v_msgs = malloc(sizeof(*layeredDecoder->c_to_v_msgs) * MESSAGE);

    for(size_t i = 0; i < MESSAGE; i++) {
        layeredDecoder->p_vn_addr[i] = &layeredDecoder->var_nodes[VN_NODE_ARRAY_NAME[i]];
    }

    return REDS_DVB_DECODER_ERROR_NONE;
}

void TERMINATE_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder)
{
    REDS_DVB_DECODER_LayeredDecoder_t* layeredDecoder = decoder->decoder;

    free(layeredDecoder->c_to_v_msgs);
    free(layeredDecoder->var_nodes);
    free(layeredDecoder->p_vn_addr);

    free(decoder->decoder);
    decoder->decoder = NULL;
}

void DECODE_FUNCTION_NAME(CODERATE_SUFFIX)(REDS_DVB_DECODER_Decoder_t* decoder,
    char var_nodes[],
    char Rprime_fix[],
    int nombre_iterations)
{
    size_t cn_idx;

    REDS_DVB_DECODER_LayeredDecoder_t* layeredDecoder = decoder->decoder;

    for(size_t i = 0; i < NOEUD; i++) {
        layeredDecoder->var_nodes[i] = (int16_t)var_nodes[i];
    }

    for(size_t i = 0; i < MESSAGE; i++) {
        layeredDecoder->c_to_v_msgs[i] = 0;
    }

    while(nombre_iterations--) {
        cn_idx = 0;

        /* For DVB-S2, there are two different CN degree */
        for(cn_idx = 0; cn_idx < DEG_1_COMPUTATIONS; cn_idx++) {
            #if DEG_1 == 7
            cn_kernel7(cn_idx, layeredDecoder->p_vn_addr, layeredDecoder->c_to_v_msgs);
            #elif DEG_1 == 27
            cn_kernel27(cn_idx, layeredDecoder->p_vn_addr, layeredDecoder->c_to_v_msgs);
            #elif DEG_1 == 30
            cn_kernel30(cn_idx, layeredDecoder->p_vn_addr, layeredDecoder->c_to_v_msgs);
            #else
            #error "Not opt"
            generic_cn_kernel(cn_idx, DEG_1, DEG_1, layeredDecoder->p_vn_addr, layeredDecoder->c_to_v_msgs);
            #endif
        }

        for(; cn_idx < DEG_1_COMPUTATIONS + DEG_2_COMPUTATIONS; cn_idx++) {
            generic_cn_kernel(cn_idx, DEG_2, DEG_1, layeredDecoder->p_vn_addr, layeredDecoder->c_to_v_msgs);
        }
    }

    /* Hard decision */
    for(size_t i = 0; i < NOEUD; i++) {
        Rprime_fix[i] = layeredDecoder->var_nodes[i] >= 0 ? 1:0;
    }
}
