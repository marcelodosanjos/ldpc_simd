/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of reds-dvb-decoder.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LAYERED_DECODER_64800X7200_DVB_S2_H
#define LAYERED_DECODER_64800X7200_DVB_S2_H

#include "reds-dvb-decoder/reds-dvb-decoder.h"

REDS_DVB_DECODER_Error_t REDS_DVB_DECODER_layered_64800x7200_dvb_s2_Init(REDS_DVB_DECODER_Decoder_t* decoder);
void REDS_DVB_DECODER_layered_64800x7200_dvb_s2_Terminate(REDS_DVB_DECODER_Decoder_t* decoder);
void REDS_DVB_DECODER_layered_64800x7200_dvb_s2_Decode(REDS_DVB_DECODER_Decoder_t* decoder,
                                     char var_nodes[],
                                     char Rprime_fix[],
                                     int nombre_iterations);

#endif /* LAYERED_DECODER_64800X7200_DVB_S2_H */
