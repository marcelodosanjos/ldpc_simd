/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of reds-dvb-decoder.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REDS_DVB_DECODER_H
#define REDS_DVB_DECODER_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stddef.h>

/**
 * Error code returned by the library.
 */
typedef enum {
    REDS_DVB_DECODER_ERROR_NONE, /**< No error. */
    REDS_DVB_DECODER_ERROR_RUNTIME, /**< Runtime error. */
    REDS_DVB_DECODER_ERROR_WRONG_PARAMETER /**< Bad arguments. */
} REDS_DVB_DECODER_Error_t;

/**
 * Decoding algorithm type.
 */
typedef enum {
    REDS_DVB_DECODER_SCHEDULER_FLOODED, /**< Flooded algorithm (see sources for ref) */
    REDS_DVB_DECODER_SCHEDULER_LAYERED /**< Layered algorithm (see sources for ref) */
} REDS_DVB_DECODER_Scheduler_t;

/**
 * Decoding coderate
 */
typedef enum {
    REDS_DVB_DECODER_CODE_DVB_S2_64800_32400, /**< 1/2 coderate */
    REDS_DVB_DECODER_CODE_DVB_S2_64800_7200, /**< 8/9 coderate */
    REDS_DVB_DECODER_CODE_DVB_S2_64800_6480 /**< 9/10 coderate */
} REDS_DVB_DECODER_Code_t;

/**
 * Decoder descriptions (do not change values directly)
 *
 */
typedef struct {
    REDS_DVB_DECODER_Scheduler_t scheduler; /**< Decoding algorithm type */
    REDS_DVB_DECODER_Code_t code; /**< Decoding coderate */
    void* decoder; /**< private data */
} REDS_DVB_DECODER_Decoder_t;

/**
 * Init function. Should be called first.
 *
 * @param decoder Pointer to a previously allocated decoder structure.
 * @param scheduler Algorithm type.
 * @param code Coderate used to decode codewords.
 *
 * @return Error code if error, REDS_DVB_DECODER_ERROR_NONE otherwise.
 */
REDS_DVB_DECODER_Error_t REDS_DVB_DECODER_Init(REDS_DVB_DECODER_Decoder_t *decoder,
                          REDS_DVB_DECODER_Scheduler_t scheduler,
                          REDS_DVB_DECODER_Code_t code);

/**
 * Terminate function. Free every allocated resources.
 *
 * @param decoder Decoder to use.
 *
 * @return Error code if error, REDS_DVB_DECODER_ERROR_NONE otherwise.
 */
REDS_DVB_DECODER_Error_t REDS_DVB_DECODER_Terminate(REDS_DVB_DECODER_Decoder_t *decoder);

/**
 * Terminate function. Free every allocated resources.
 *
 * @param decoder Decoder to use.
 * @param input Input codeword. Should be 64800 byte long for DVB-S2.
 * @param output Output codeword. Should be 64800 byte long for DVB-S2.
 * @param iterationCount Number of decoding iterations.
 *
 * @return Error code if error, REDS_DVB_DECODER_ERROR_NONE otherwise.
 */
REDS_DVB_DECODER_Error_t REDS_DVB_DECODER_Decode(REDS_DVB_DECODER_Decoder_t *decoder,
                            char input[],
                            char output[],
                            int iterationCount);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* REDS_DVB_DECODER_H */
