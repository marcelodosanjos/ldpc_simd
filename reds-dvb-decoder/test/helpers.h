#ifndef _HELPERS_H
#define _HELPERS_H

#include <reds-dvb-decoder/reds-dvb-decoder.h>

/**
 * \return First wrong index found in output, -1 if no error, -2 if runtime error.
 */
int test_decode_helper(REDS_DVB_DECODER_Decoder_t* decoder,
                        char initValue,
                        unsigned int iterationMin,
                        unsigned int iterationMax,
                        unsigned int codewordLength);

/**
 * \return First wrong index found in output, -1 if no error, -2 if runtime error.
 */
int test_decode_with_error_helper(REDS_DVB_DECODER_Decoder_t* decoder,
                        char initValue,
                        unsigned int iterationMin,
                        unsigned int iterationMax,
                        unsigned int codewordLength,
                        unsigned int posError,
                        char errorValue);

#endif /* _HELPERS_H */
