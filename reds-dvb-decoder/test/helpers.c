#include "helpers.h"

#include <stdlib.h>
#include <string.h>

int test_decode_helper(REDS_DVB_DECODER_Decoder_t* decoder,
                        char initValue,
                        unsigned int iterationMin,
                        unsigned int iterationMax,
                        unsigned int codewordLength)
{
    return test_decode_with_error_helper(decoder,
                                         initValue,
                                         iterationMin,
                                         iterationMax,
                                         codewordLength,
                                         0,
                                         initValue);
}

int test_decode_with_error_helper(REDS_DVB_DECODER_Decoder_t* decoder,
                        char initValue,
                        unsigned int iterationMin,
                        unsigned int iterationMax,
                        unsigned int codewordLength,
                        unsigned int posError,
                        char errorValue)
{
    int retval = -1;
    REDS_DVB_DECODER_Error_t ret;
    char* i_llr = malloc(sizeof(*i_llr) * codewordLength);
    char* o_llr = malloc(sizeof(*o_llr) * codewordLength);

    for (unsigned int iter = iterationMin ; iter < iterationMax ; iter++)
    {
        memset(i_llr, initValue, codewordLength);
        i_llr[posError] = errorValue;

        ret = REDS_DVB_DECODER_Decode(decoder, i_llr, o_llr, iter);
        if (ret != REDS_DVB_DECODER_ERROR_NONE)
        {
            retval = -2;
            break;
        }

        for (unsigned int i = 0; i < codewordLength ; i++)
        {
            if (o_llr[i] != 0) {
                retval = i;
                break;
            }
        }
    }

    free(i_llr);
    free(o_llr);

    return retval;
}
