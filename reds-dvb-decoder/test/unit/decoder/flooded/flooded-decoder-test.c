#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <reds-dvb-decoder/reds-dvb-decoder.h>

#include "helpers.h"

#define ITERATION_MIN 1
#define ITERATION_MAX 20

#define CODEWORD_LENGTH 64800

#define NB_BITS_VARIABLES    8
#define NB_BITS_MESSAGES     6
#define SAT_POS_VAR  ( (0x0001<<(NB_BITS_VARIABLES-1))-1)
#define SAT_NEG_VAR  (-(0x0001<<(NB_BITS_VARIABLES-1))+1)
#define SAT_POS_MSG  ( (0x0001<<(NB_BITS_MESSAGES -1))-1)
#define SAT_NEG_MSG  (-(0x0001<<(NB_BITS_MESSAGES -1))+1)

int setup_flooded_1_2(void** state)
{
    REDS_DVB_DECODER_Error_t ret;

    REDS_DVB_DECODER_Decoder_t* decoder = malloc(sizeof(REDS_DVB_DECODER_Decoder_t));

    ret = REDS_DVB_DECODER_Init(decoder,
                                REDS_DVB_DECODER_SCHEDULER_FLOODED,
                                REDS_DVB_DECODER_CODE_DVB_S2_64800_32400);
    if (ret != REDS_DVB_DECODER_ERROR_NONE)
    {
        free(decoder);
        fprintf(stderr, "Error: REDS_DVB_DECODER_Init failed: %d\n", ret);
        return -1;
    }

    *state = decoder;

    return 0;
}

int setup_flooded_8_9(void** state)
{
    REDS_DVB_DECODER_Error_t ret;

    REDS_DVB_DECODER_Decoder_t* decoder = malloc(sizeof(REDS_DVB_DECODER_Decoder_t));

    ret = REDS_DVB_DECODER_Init(decoder,
                                REDS_DVB_DECODER_SCHEDULER_FLOODED,
                                REDS_DVB_DECODER_CODE_DVB_S2_64800_7200);
    if (ret != REDS_DVB_DECODER_ERROR_NONE)
    {
        free(decoder);
        fprintf(stderr, "Error: REDS_DVB_DECODER_Init failed: %d\n", ret);
        return -1;
    }

    *state = decoder;

    return 0;
}

int setup_flooded_9_10(void** state)
{
    REDS_DVB_DECODER_Error_t ret;

    REDS_DVB_DECODER_Decoder_t* decoder = malloc(sizeof(REDS_DVB_DECODER_Decoder_t));

    ret = REDS_DVB_DECODER_Init(decoder,
                                REDS_DVB_DECODER_SCHEDULER_FLOODED,
                                REDS_DVB_DECODER_CODE_DVB_S2_64800_6480);
    if (ret != REDS_DVB_DECODER_ERROR_NONE)
    {
        free(decoder);
        fprintf(stderr, "Error: REDS_DVB_DECODER_Init failed: %d\n", ret);
        return -1;
    }

    *state = decoder;

    return 0;
}

int teardown(void** state)
{
    REDS_DVB_DECODER_Error_t ret;

    REDS_DVB_DECODER_Decoder_t* decoder = *state;

    ret = REDS_DVB_DECODER_Terminate(decoder);
    if (ret != REDS_DVB_DECODER_ERROR_NONE)
    {
        fprintf(stderr, "Error: REDS_DVB_DECODER_Terminate failed: %d\n", ret);
        return -1;
    }

    free(*state);
    *state = NULL;

    return 0;
}

void flooded_decoder_zeroed_input_test(void** state)
{
    int ret;
    REDS_DVB_DECODER_Decoder_t* decoder;

    decoder = *state;

    ret = test_decode_helper(decoder,
                             SAT_NEG_MSG,
                             ITERATION_MIN,
                             ITERATION_MAX,
                             CODEWORD_LENGTH);
    assert_true(ret == -1);
}

void flooded_decoder_zeroed_input_with_first_flipped_bit_test(void** state)
{
    int ret;
    REDS_DVB_DECODER_Decoder_t* decoder;

    decoder = *state;

    ret = test_decode_with_error_helper(decoder,
                                        SAT_NEG_MSG,
                                        ITERATION_MIN,
                                        ITERATION_MAX,
                                        CODEWORD_LENGTH,
                                        0,
                                        SAT_POS_MSG);
    assert_true(ret == -1);
}

void flooded_decoder_zeroed_input_with_last_flipped_bit_test(void** state)
{
    int ret;
    REDS_DVB_DECODER_Decoder_t* decoder;

    decoder = *state;

    ret = test_decode_with_error_helper(decoder,
                                        SAT_NEG_MSG,
                                        ITERATION_MIN,
                                        ITERATION_MAX,
                                        CODEWORD_LENGTH,
                                        CODEWORD_LENGTH - 1,
                                        SAT_POS_MSG);
    assert_true(ret == -1);
}

void flooded_decoder_zeroed_input_with_random_flipped_bit_test(void** state)
{
    int ret;
    REDS_DVB_DECODER_Decoder_t* decoder;

    decoder = *state;

    ret = test_decode_with_error_helper(decoder,
                                        SAT_NEG_MSG,
                                        ITERATION_MIN,
                                        ITERATION_MAX,
                                        CODEWORD_LENGTH,
                                        CODEWORD_LENGTH / 4,
                                        SAT_POS_MSG);
    assert_true(ret == -1);
}

int main(void)
{
    const struct CMUnitTest tests [] =
    {
        /* 1/2 coderate */
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_test, setup_flooded_1_2, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_first_flipped_bit_test, setup_flooded_1_2, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_last_flipped_bit_test, setup_flooded_1_2, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_random_flipped_bit_test, setup_flooded_1_2, teardown),

        /* 8/9 coderate */
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_test, setup_flooded_8_9, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_first_flipped_bit_test, setup_flooded_8_9, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_last_flipped_bit_test, setup_flooded_8_9, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_random_flipped_bit_test, setup_flooded_8_9, teardown),

        /* 9/10 coderate */
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_test, setup_flooded_9_10, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_first_flipped_bit_test, setup_flooded_9_10, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_last_flipped_bit_test, setup_flooded_9_10, teardown),
        cmocka_unit_test_setup_teardown(flooded_decoder_zeroed_input_with_random_flipped_bit_test, setup_flooded_9_10, teardown),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
